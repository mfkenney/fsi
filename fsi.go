// Fsi provides a communication interface for FSI Acoustic Current Meters
package fsi

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
)

const (
	// Command terminator
	EOL = "\r"
	// Command prompt
	PROMPT = "\r\n"
	// Data record terminator in streaming mode
	EOR = "\r\n"
)

var BadCommand = errors.New("Invalid command")

type Device struct {
	port     io.ReadWriter
	respBuf  bytes.Buffer
	rmu, wmu *sync.Mutex
	lastCmd  string
}

func NewDevice(port io.ReadWriter) *Device {
	return &Device{
		port: port,
		rmu:  &sync.Mutex{},
		wmu:  &sync.Mutex{}}
}

func (d *Device) readUntil(marker []byte) (string, error) {
	d.rmu.Lock()
	defer d.rmu.Unlock()

	d.respBuf.Reset()
	tail := -len(marker)
	b := make([]byte, 1)
	for {
		n, err := d.port.Read(b)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}

		if err != nil {
			return d.respBuf.String(), err
		}

		d.respBuf.Write(b)
		tail++
		if tail < 0 {
			continue
		}

		if bytes.Equal(d.respBuf.Bytes()[tail:], marker) {
			return strings.TrimSuffix(d.respBuf.String(), string(marker)), nil
		}
	}

}

// Send sends a command to the Device
func (d *Device) Send(cmd string) error {
	d.wmu.Lock()
	defer d.wmu.Unlock()

	d.lastCmd = cmd
	var err error

	_, err = fmt.Fprint(d.port, cmd, EOL)

	return err
}

// Stop takes the device out of run (streaming) mode
func (d *Device) Stop() error {
	d.wmu.Lock()
	defer d.wmu.Unlock()
	_, err := fmt.Fprint(d.port, "S")
	return err
}

// Start puts the Device into streaming mode
func (d *Device) Start() error {
	err := d.Send("RUN")
	if err != nil {
		return err
	}
	_, err = d.Recv()
	return err
}

// Recv returns a response from the Device.
func (d *Device) Recv() (string, error) {
	resp, err := d.readUntil([]byte(PROMPT))
	if err != nil {
		return "", err
	}

	if strings.HasPrefix(resp, "BAD COMMAND") {
		return resp, fmt.Errorf("%q: %w", d.lastCmd, BadCommand)
	}

	return resp, nil
}

// Exec sends a command to the Device and returns the response along with
// any error that occurs.
func (d *Device) Exec(cmd string) (string, error) {
	err := d.Send(cmd)
	if err != nil {
		return "", err
	}
	return d.Recv()
}

// Stream returns a channel which will supply data records when the Device
// is in "streaming mode".
func (d *Device) Stream(ctx context.Context) <-chan string {
	ch := make(chan string, 1)
	go func() {
		defer close(ch)
		for {
			text, err := d.readUntil([]byte(EOR))
			if err != nil {
				return
			}
			select {
			case <-ctx.Done():
				return
			case ch <- text:
			default:
			}
		}
	}()

	return ch
}
